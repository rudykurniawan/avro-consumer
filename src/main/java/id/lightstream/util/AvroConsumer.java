package id.lightstream.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

public class AvroConsumer {
	public static List<String> retrieveMessages(
    		String URI_BOOTSTRAP_SERVER, 
    		String URL_SCHEMA_REGISTRY, 
    		String TOPIC_NAME, 
    		String CONSUMER_GROUP_ID,
    		String AUTO_OFFSET_RESET) {
    	
    	final Properties props = new Properties();
    	props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, org.apache.kafka.common.serialization.StringDeserializer.class);
    	props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, io.confluent.kafka.serializers.KafkaAvroDeserializer.class);
    	props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, URI_BOOTSTRAP_SERVER);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, CONSUMER_GROUP_ID);
        props.put("schema.registry.url", URL_SCHEMA_REGISTRY);
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, AUTO_OFFSET_RESET);
        /*
         * AUTO_OFFSET_RESET_CONFIG
         * earliest: automatically reset the offset to the earliest offset
         * latest: automatically reset the offset to the latest offset
         * none: throw exception to the consumer if no previous offset is found for the consumer's group
         * anything else: throw exception to the consumer.
         */

        final KafkaConsumer<String, GenericRecord> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Collections.singletonList(TOPIC_NAME));
        
        if(AUTO_OFFSET_RESET.toLowerCase().equals("earliest")) {
        	consumer.poll(0);
        	consumer.seekToBeginning(consumer.assignment());
        }
        
        List<String> messagesList = new ArrayList<String>();
        ConsumerRecords<String, GenericRecord> consumerRecords = consumer.poll(1000);
        try {
        	for (ConsumerRecord<String, GenericRecord> consumerRecord : consumerRecords) {
                messagesList.add(consumerRecord.value().toString());
            }
		} finally {
			consumer.close();
		}
        return messagesList;
    }
}
