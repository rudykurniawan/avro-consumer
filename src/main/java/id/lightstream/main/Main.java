package id.lightstream.main;

import id.lightstream.util.AvroConsumer;

public class Main {
	
    public static void main(String[] args) {
        AvroConsumer consumer = new AvroConsumer();
    	System.out.println(consumer.retrieveMessages("192.168.1.93:9092", "http://192.168.1.93:8081", "sgc_timeless", "test-consumer-group", "earliest").toString());
    }
}
